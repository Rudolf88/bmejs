/**
 * Created by erudhor on 2016.05.06..
 */
var nodemailer = require('nodemailer');
var sgTransport = require('nodemailer-sendgrid-transport');
var secret = require('./config/secret.js');

var options = {
    auth: {
        api_key: secret.sendgridAuth.api_key
    }
};

var mailer = nodemailer.createTransport(sgTransport(options));


exports.mailer = mailer;