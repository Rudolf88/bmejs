/**
 * Created by Rudolf on 28/03/2016.
 */

var passport = require('passport');
var FacebookStrategy = require('passport-facebook').Strategy;
var User = require('./models/user.js');


module.exports = function(app) {

    // Call it after port and running_mode is given,
    // to create proper callback link
    var secret = require('./config/secret.js');


    // Set up Passport authentication
    passport.use(new FacebookStrategy({
            clientID: secret.facebookAuth.clientID,
            clientSecret: secret.facebookAuth.clientSecret,
            callbackURL: secret.facebookAuth.callbackURL
        },
        function (accessToken, refreshToken, profile, done) {
            User.findOrCreate(accessToken, refreshToken, profile, function (err, user) {
                if (err) {
                    return done(err);
                }
                done(null, user);
            });
        }
    ));

    passport.serializeUser(function (user, done) {
        //console.log('serializeUser: ' + user);
        done(null, user);
    });

    passport.deserializeUser(function (id, done) {
        User.findById(id, function (err, user) {
            //console.log(user);
            if (!err) done(null, user);
            else done(err, null)
        })
    });

};