var expect = require('chai').expect;
var sendMessageMW = require('../../../middleware/subscription/send_message');

describe('sendMessage middleware ', function () {

    describe('should send error message when', function(){
        it('no message found', function(done){
            var trap = false;

            var fakeMailer = {
                sendMail: function (email, cb) {
                    trap = true;
                    cb();
                }
            };

            var res = {
                tpl: {
                    error: []
                }
            };

            sendMessageMW({
                mailer: fakeMailer
            })({}, res, function (err) {
                expect(err).to.eql(undefined);
                expect(trap).to.be.eql(false);
                expect(res.tpl.error).not.to.be.eql([]);
                done();
            });
        });

        it('message has no subject', function(done){
            var trap = false;

            var fakeMailer = {
                sendMail: function (email, cb) {
                    trap = true;
                    cb();
                }
            };

            var res = {
                tpl: {
                    msg:{
                        //subject: "subject...",
                        content: "conent..."
                    },
                    error: []
                }
            };

            sendMessageMW({
                mailer: fakeMailer
            })({}, res, function (err) {
                expect(err).to.eql(undefined);
                expect(trap).to.be.eql(false);
                expect(res.tpl.error).not.to.be.eql([]);
                done();
            });
        });

        it('message has no content', function(done){
            var trap = false;

            var fakeMailer = {
                sendMail: function (email, cb) {
                    trap = true;
                    cb();
                }
            };

            var res = {
                tpl: {
                    msg:{
                        subject: "subject..."
                        //content: "conent..."
                    },
                    error: []
                }
            };

            sendMessageMW({
                mailer: fakeMailer
            })({}, res, function (err) {
                expect(err).to.eql(undefined);
                expect(trap).to.be.eql(false);
                expect(res.tpl.error).not.to.be.eql([]);
                done();
            });
        });

        it('error occured during sending', function(done){
            var trap = false;

            var fakeMailer = {
                sendMail: function (email, cb) {
                    trap = true;
                    cb("problem");
                }
            };

            var res = {
                tpl: {
                    msg:{
                        subject: "subject...",
                        content: "content..."
                    },
                    error: []
                }
            };

            sendMessageMW({
                mailer: fakeMailer
            })({}, res, function (err) {
                expect(err).to.eql(undefined);
                expect(trap).to.be.eql(true);
                expect(res.tpl.error).not.to.be.eql([]);
                done();
            });
        });
    });

    it('should send email when everything is OK', function(done){
        var trap = false;

        var example_msg = {
                subject: "subject...",
                content: "content..."
            };

        var fakeMailer = {
            sendMail: function (email, cb) {
                trap = true;
                expect(email).to.have.property('subject', 'subject...');
                expect(email).to.have.property('text', 'content...');
                cb(undefined, "OK");
            }
        };

        var res = {
            tpl: {
                msg: example_msg,
                error: []
            }
        };

        sendMessageMW({
            mailer: fakeMailer
        })({}, res, function (err) {
            expect(err).to.eql(undefined);
            expect(trap).to.be.eql(true);
            expect(res.tpl.error).to.be.eql([]);
            done();
        });
    });
});
