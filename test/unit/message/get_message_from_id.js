var expect = require('chai').expect;
var getMessageFromIdMW = require('../../../middleware/message/get_message_from_id');

describe('getMessageFromId middleware ', function () {

    describe('should set empty message and call next when', function(){
        it('no parameter id in post', function(done){
            var trap = false;

            var fakeMsgModel = {
                findOne: function (some, cb) {
                    trap = true;
                    cb();
                }
            };

            var res = {
                tpl: {}
            };

            var req = {
                param: function(key){
                    return undefined
                },
                body: {}
            };

            var empty_msg = {
                name: "",
                _id:"",
                content: "",
                subject:""
            };

            getMessageFromIdMW({
                msgModel: fakeMsgModel
            })(req, res, function (err) {
                expect(trap).to.be.eql(false);
                expect(err).to.eql(undefined);
                expect(res.tpl.msg).to.be.eql(empty_msg);
                done();
            });
        });
    });

    describe('should set empty message and call next when', function(){
        it('parameter id in post is null', function(done){
            var trap = false;

            var fakeMsgModel = {
                findOne: function (some, cb) {
                    trap = true;
                    cb();
                }
            };

            var res = {
                tpl: {}
            };

            var req = {
                param: function(key){
                    if (key == 'id')
                        return null;
                    trap = true;
                    return undefined;
                },
                body: {}
            };

            var empty_msg = {
                name: "",
                _id:"",
                content: "",
                subject:""
            };

            getMessageFromIdMW({
                msgModel: fakeMsgModel
            })(req, res, function (err) {
                expect(trap).to.be.eql(false);
                expect(err).to.eql(undefined);
                expect(res.tpl.msg).to.be.eql(empty_msg);
                done();
            });
        });
    });


    it('should pass message if everything is ok', function (done) {

        var id = 11;
        var msg = "cucc";

        var fakeMsgModel = function(){};

        fakeMsgModel.findOne =  function (db_filter, cb) {
            if (db_filter._id == id){
                return cb(undefined, msg);
            }
            cb();
        };

        var res = {
            tpl: {
                error: []
            }
        };

        var req = {
            param: function(key){
                if (key == 'id')
                    return id;
            },
            body: {}
        };

        getMessageFromIdMW({
            msgModel: fakeMsgModel
        })(req, res, function (err) {
            expect(err).to.eql(undefined);
            expect(res.tpl.error).to.eql([]);
            expect(res.tpl.msg).to.be.eql(msg);
            done();
        });
    });


    describe('should pass empty message when', function(){

        it('an error occured', function (done) {
            var id = 11;

            var fakeMsgModel = function(){};

            fakeMsgModel.findOne =  function (db_filter, cb) {
                cb("error");
            };

            var res = {
                tpl: {
                    error: []
                }
            };

            var req = {
                param: function(key){
                    if (key == 'id')
                        return id;
                },
                body: {}
            };

            var empty_msg = {
                name: "",
                _id:"",
                content: "",
                subject:""
            };

            getMessageFromIdMW({
                msgModel: fakeMsgModel
            })(req, res, function (err) {
                expect(err).to.eql(undefined);
                expect(res.tpl.error).not.to.be.eql([]);
                expect(res.tpl.msg).to.be.eql(empty_msg);
                done();
            });
        });

        it('message is not found', function (done) {

            var id = 11;

            var fakeMsgModel = function(){};

            fakeMsgModel.findOne =  function (db_filter, cb) {
                cb(undefined, undefined);
            };

            var res = {
                tpl: {
                    error: []
                }
            };

            var req = {
                param: function(key){
                    if (key == 'id')
                        return id;
                },
                body: {}
            };

            var empty_msg = {
                name: "",
                _id:"",
                content: "",
                subject:""
            };

            getMessageFromIdMW({
                msgModel: fakeMsgModel
            })(req, res, function (err) {
                expect(err).to.eql(undefined);
                expect(res.tpl.error).not.to.be.eql([]);
                expect(res.tpl.msg).to.be.eql(empty_msg);
                done();
            });
        });
    });
});
