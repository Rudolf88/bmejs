var Schema = require('mongoose').Schema;
var db = require('../config/db');

var Message = db.model('Message', {
    //_id: Schema.Types.ObjectId,
    subject: String,
    content: String,
    name: String
});

module.exports = Message;