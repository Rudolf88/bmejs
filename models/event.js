var Schema = require('mongoose').Schema;
var db = require('../config/db');

var Event = db.model('Event', {
    name: String,
    img: String,
    date: String,
    time: String,
    details: String,
    place: String,
    active: Boolean
});

module.exports = Event;