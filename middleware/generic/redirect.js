/**
 * Created by Rudolf on 29/03/2016.
 */


module.exports = function (url) {

    return function (req, res, next) {
        return res.redirect(url);
    }

};
