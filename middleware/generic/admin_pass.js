/**
 * Created by Rudolf on 28/03/2016.
 */


var requireOption = require('../common').requireOption;

/**
 * Checks if user is logged in
 */
module.exports = function (objectrepository) {

    return function (req, res, next) {
        if (req.user) {
            res.tpl.logged_in = true;
            res.tpl.username = req.user;
            console.log(req.user);
            return next();
        } else {
            //return next();
            res.redirect('/login');
        }
    };

};