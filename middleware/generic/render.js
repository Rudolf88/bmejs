/**
 * Created by Rudolf on 28/03/2016.
 */
/**
 * Using the template engine render the values into the template
 */
module.exports = function (objrepo, viewName) {

    return function (req, res) {
        //console.log("Render content: "+JSON.stringify(res.tpl));
        res.render(viewName, res.tpl);
    };

};
