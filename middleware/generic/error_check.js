
module.exports = function (objrepo) {

    return function (req, res) {
        if(res.tpl.error.length > 0){
            console.log("Errorous call: "+JSON.stringify(res.tpl.error));
            res.statusCode = 400;
            res.end(JSON.stringify(res.tpl.error));
        }else{
            if(typeof  res.tpl.answer === "undefined"){
                res.tpl.answer = "Answer not given..."
            }
            res.end(JSON.stringify(res.tpl.answer));
        }
    };

};
