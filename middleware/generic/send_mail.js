/**
 * Created by Rudolf on 29/03/2016.
 */

var requireOption = require('../common').requireOption;


module.exports = function (objectrepository) {

    return function (req, res, next) {
        /*
         * From the Welcome page users can send email to the Church members for pray, etc...
         * With:
         *  - message body
         *  - message type
         *  - sender email
         *  - message subject
         *  - (receiver is wired to message type)
         * */
        return next();
    };

};