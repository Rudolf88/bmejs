/**
 * Created by Rudolf on 29/03/2016.
 */

module.exports = function (objectrepository) {

    return function(req, res){
        req.logout();
        res.redirect('/');
    };

};