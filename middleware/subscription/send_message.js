/**
 * Created by Rudolf on 29/03/2016.
 */

var requireOption = require('../common').requireOption;


module.exports = function (objrepo) {

    var mailer = requireOption(objrepo, 'mailer');

    return function (req, res, next) {
        /*
         * Send out given message sample to the subscribed email addresses
         * */
        if (res.tpl.msg == undefined ){
            res.tpl.error.push("No message found to send! (no id sent? message with id not found?)");
            return next();
        }
        if(typeof(res.tpl.msg.subject) != 'string'){
            res.tpl.error.push("Message has no subject!");
            return next();
        }
        if(typeof(res.tpl.msg.content) != 'string'){
            res.tpl.error.push("Message has no subject!");
            return next();
        }
        console.log("Sending message: "+res.tpl.msg._id);


        var email = {
            from: 'test@bmejs.hu',
            to: 'rudolf.official@gmail.com',
            subject: res.tpl.msg.subject,
            text: res.tpl.msg.content
            //, html: '<b>Hello world</b>'
        };

        mailer.sendMail(email, function(err, info){
            if (err){
                console.log("Error sending the message: "+err);
                res.tpl.error.push("Failed to send the message!");
                return next();
            }
            else {
                console.log('Message sent: ' + info.response);
                res.tpl.answer = 'Message sent: ' + info.response;
                return next();
            }
        });
    };
};