/**
 * Created by Rudolf on 28/03/2016.
 */

var requireOption = require('../common').requireOption;


module.exports = function (objrepo) {

    var eventModel = requireOption(objrepo, 'eventModel');

    return function (req, res, next) {
        /*
         * deletes event by ID
         * */
        if(res.tpl.event._id == ""){
            console.log("No event found");
            res.tpl.error.push("Event id not given or message not found!");
            return next();
        }
        console.log("Delete event: "+res.tpl.event._id);

        res.tpl.event.remove(function (err, result) {
            if (err) {
                console.log("Deleting event failed: " + JSON.stringify(err));
                res.tpl.error.push("Deleting event failed: " + JSON.stringify(err));
                return next();
            }
            res.tpl.answer = "OK";
            console.log("Event deleted!");

            return next();
        });
    };

};