/**
 * Created by Rudolf on 28/03/2016.
 */

var requireOption = require('../common').requireOption;

module.exports = function (objrepo) {

    var eventModel = requireOption(objrepo, 'eventModel');

    var new_event = {
        name: "",
        _id: "",
        date: "",
        time: "",
        details: "",
        img: "",
        place: "",
        activate: false
    };

    return function (req, res, next) {
        /*
         * Find Event by ID
         * */
        if (typeof req.param('id') === "undefined"){
            console.log("Nincs ID szállítva (url path)");
        }else{
            console.log("Van ID szállítva: "+req.param('id'));
            req.body._id = req.param('id');
        }

        if(req.body._id == undefined || req.body._id == null){
            res.tpl.event = new_event;
            return next();
        }
        if (req.body._id == ""){
            res.tpl.event = req.body;
            console.log("Event sent to server: "+JSON.stringify(req.body));
            return next();
        }

        event = new eventModel();

        console.log("Found Event ID: "+req.body._id);

        eventModel.findOne({
            _id: req.body._id
        }, function (err, result) {
            if ((err) || (!result)) {
                console.log("Event not found in database by ID");
                res.tpl.error.push("Could not find existing event: "+JSON.stringify(err));
                res.tpl.event = new_event;
                return next();
            }
            console.log("Event found by ID");
            res.tpl.event = result;
            return next();
        });
    };
};