/**
 * Created by Rudolf on 28/03/2016.
 */

var requireOption = require('../common').requireOption;


module.exports = function (objrepo) {

    var eventModel = requireOption(objrepo, 'eventModel');

    return function (req, res, next) {
        /*
         * Creates event if ID is given
         * */

        if(typeof res.tpl.event === "undefined"){
            console.log("No event given");
            return next();
        }
        var event_data = res.tpl.event;
        if(event_data._id != ""){
            console.log("No creating of an event (modify?)");
            return next();
        }

        console.log("Create event: "+JSON.stringify(event_data));

        var event = new eventModel();
        event.name = event_data.name;
        event.date = event_data.date;
        event.place= event_data.place;
        event.time = event_data.time;
        event.img  = event_data.img;
        event.details = event_data.details;
        event.active  = event_data.active;

        event.save(function (err, result) {
            if (err) {
                console.log("Saving event failed: " + JSON.stringify(err));
                res.tpl.error.push("Saving event failed: " + JSON.stringify(err));
                return next();
            }
            res.tpl.answer = result._id;
            res.tpl.redirect = "/admin/event/edit/"+result._id;
            console.log("New Event saved: ID=" + result._id);

            return next();
        });
    };

};