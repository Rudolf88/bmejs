/**
 * Created by Rudolf on 28/03/2016.
 */

var requireOption = require('../common').requireOption;

module.exports = function (objrepo) {

    var eventModel = requireOption(objrepo, 'eventModel');

    return function (req, res, next) {
        /*
        * returns 3 upcoming events
        * Just some details:
        *   - picture
        *   - name
        *   - link(id)
        * */

        eventModel.find({}, function (err, docs) {
            if ((err) || (!docs)) {
                console.log("Error getting messages list!");
                res.tpl.error.push("Could not find list of messages: "+JSON.stringify(err));
                return next();
            }
            res.tpl.events = docs;
            return next();
        });
    };

};