/**
 * Created by Rudolf on 29/03/2016.
 */
var requireOption = require('../common').requireOption;

module.exports = function (objrepo) {

    var eventModel = requireOption(objrepo, 'eventModel');

    return function (req, res, next) {
        /*
         * It can activate an event or deactivate
         * */
        if(typeof res.tpl.event === "undefined"){
            return next();
        }
        if(res.tpl.event._id == ""){
            console.log("Create event (not modify)");
            return next();
        }
        console.log("Modify event: "+res.tpl.event._id);


        // Modify existing Message
        var event_data = req.body;
        res.tpl.event.name = event_data.name;
        res.tpl.event.date = event_data.date;
        res.tpl.event.place= event_data.place;
        res.tpl.event.time = event_data.time;
        res.tpl.event.img  = event_data.img;
        res.tpl.event.details = event_data.details;
        res.tpl.event.active  = event_data.active;

        res.tpl.event.save(function (err, result) {
            if (err) {
                console.log("Saving Event failed: " + JSON.stringify(err));
                res.tpl.error.push("Saving event failed: " + JSON.stringify(err));
                return next();
            }
            res.tpl.answer = result._id;
            console.log("Event modified: ID=" + result._id);

            return next();
        });
    };

};