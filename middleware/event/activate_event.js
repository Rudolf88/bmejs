/**
 * Created by Rudolf on 09/05/2016.
 */

var requireOption = require('../common').requireOption;

module.exports = function (objrepo) {

    var eventModel = requireOption(objrepo, 'eventModel');

    return function (req, res, next) {
        /*
         * It can activate an event or deactivate
         * */
        if(typeof res.tpl.event === "undefined"){
            return next();
        }
        if(res.tpl.event._id == ""){
            console.log("Create event (not modify)");
            return next();
        }
        console.log("De/Activate event: "+res.tpl.event._id);
        console.log("new state: "+req.body.active);


        // De/Activate existing Message
        var event_data = req.body;
        res.tpl.event.active  = event_data.active;

        res.tpl.event.save(function (err, result) {
            if (err) {
                console.log("Saving Event failed: " + JSON.stringify(err));
                res.tpl.error.push("Saving event failed: " + JSON.stringify(err));
                return next();
            }
            res.tpl.answer = result._id;
            console.log("Event modified: ID=" + result._id);

            return next();
        });
    };

};