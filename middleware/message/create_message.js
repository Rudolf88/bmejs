/**
 * Created by Rudolf on 06/05/2016.
 */
/**
 * Created by Rudolf on 28/03/2016.
 */

var requireOption = require('../common').requireOption;


module.exports = function (objrepo) {

    var msgModel = requireOption(objrepo, 'msgModel');


    return function (req, res, next) {
        /*
         * Create message
         * */
        requirement = [req.body.subject, req.body.content, req.body.name];
        accepted = ! requirement.every(function(k){ return typeof k !== "undefined" });
        if(accepted){
            res.tpl.error.push("Not every parameter sent! (subject, content, name)");
            return next();
        }

        if(res.tpl.msg._id != "") {
            return next();
        }

        console.log("Create new Message");
        msg = new msgModel();
        msg.content = req.body.content;
        msg.subject = req.body.subject;
        msg.name    = req.body.name;

        msg.save(function (err, result) {
            if (err) {
                console.log("Saving message failed: " + JSON.stringify(err));
                res.tpl.error.push("Saving message failed: " + JSON.stringify(err));
                return next();
            }
            res.tpl.answer = result._id;
            res.tpl.redirect = "/admin/messages/edit/"+result._id;
            console.log("New Message saved: ID=" + result._id);

            return next();
        });

    };
};