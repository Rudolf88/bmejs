/**
 * Created by Rudolf on 28/03/2016.
 */

var requireOption = require('../common').requireOption;


module.exports = function (objrepo) {

    var msgModel = requireOption(objrepo, 'msgModel');

    return function (req, res, next) {
        /*
         * deletes message sample by ID
         * */
        if(res.tpl.msg == undefined){
            console.log("No message found");
            res.tpl.error.push("Message id not given or message not found!");
            return next();
        }
        console.log("Delete message: "+res.tpl.msg._id);



        res.tpl.msg.remove(function (err, result) {
            if (err) {
                console.log("Deleting message failed: " + JSON.stringify(err));
                res.tpl.error.push("Deleting message failed: " + JSON.stringify(err));
                return next();
            }
            res.tpl.answer = "OK";
            console.log("Message deleted!");

            return next();
        });
    };

};