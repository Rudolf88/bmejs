/**
 * Created by Rudolf on 28/03/2016.
 */

var requireOption = require('../common').requireOption;


module.exports = function (objrepo) {

    var msgModel = requireOption(objrepo, 'msgModel');


    return function (req, res, next) {
        /*
         * Modify by message sample by ID
         * If ID is null, creates it.
         * */
        if(req.body.subject == undefined){
            res.tpl.error.push("No subject parameter sent!");
            return next();
        }
        if(req.body.content == undefined){
            res.tpl.error.push("No content parameter sent!");
            return next();
        }

        if(res.tpl.msg == undefined ||
            res.tpl.msg._id == "") {
            return next();
        }

        // Modify existing Message
        res.tpl.msg.content = req.body.content;
        res.tpl.msg.subject = req.body.subject;
        res.tpl.msg.name    = req.body.name;

        res.tpl.msg.save(function (err, result) {
            if (err) {
                console.log("Saving message failed: " + JSON.stringify(err));
                res.tpl.error.push("Saving message failed: " + JSON.stringify(err));
                return next();
            }
            res.tpl.answer = result._id;
            console.log("Message modified: ID=" + result._id);

            return next();
        });
    };
};