/**
 * Created by Rudolf on 28/03/2016.
 */

var requireOption = require('../common').requireOption;


module.exports = function (objrepo) {

    var msgModel = requireOption(objrepo, 'msgModel');

    var empty_msg = {
        name: "",
        _id:"",
        content: "",
        subject:""
    };

    return function (req, res, next) {
        /*
         * Find message by ID
         * */
        if (typeof req.param('id') === "undefined"){
            console.log("Nincs ID szállítva (url path)");
        }else{
            console.log("Van ID szállítva: "+req.param('id'));
            req.body.id = req.param('id');
        }

        if(req.body.id == undefined || req.body.id == null){
            res.tpl.msg = empty_msg;
            return next();
        }

        console.log("Found Message ID: "+req.body.id);

        msgModel.findOne({
                _id: req.body.id
        }, function (err, result) {
                if ((err) || (!result)) {
                    console.log("Message not found by ID");
                    res.tpl.error.push("Could not find existing message: "+JSON.stringify(err));
                    res.tpl.msg = empty_msg;
                    return next();
                }
                console.log("Message found by ID");
                res.tpl.msg = result;
                return next();
        });
    };
};