/**
 * Created by Rudolf on 28/03/2016.
 */

var requireOption = require('../common').requireOption;


module.exports = function (objrepo) {

    var msgModel = requireOption(objrepo, 'msgModel');

    return function (req, res, next) {
        /*
         * Get Sample messages: ID and name pairs
         * */
        msg = new msgModel();

        msgModel.find({}, function (err, docs) {
            if ((err) || (!docs)) {
                console.log("Error getting messages list!");
                res.tpl.error.push("Could not find list of messages: "+JSON.stringify(err));
                return next();
            }
            res.tpl.messages = docs;
            return next();
        });
    };

};