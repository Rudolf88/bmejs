var express = require('express');
var passport = require('passport');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var session = require('express-session');

var passport_init = require('./passport_init.js');
var nodemailer_init = require('./nodemailer_init.js');

var auth = require('./routes/auth.js');
var event = require('./routes/event.js');
var welcome = require('./routes/welcome.js');

default_port = 9090;

var app = express();

// Set up EJS as templating system
app.set('view engine', 'ejs');

//Use npm libraries
app.use(cookieParser());
//app.use(bodyParser());
app.use(bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
}));
app.use(session({
    secret: ';ahdfgj cat'
}));
app.use(passport.initialize());
app.use(passport.session());


/**
 * Let's create the .tpl and .error on the res object
 */
app.use(function (req, res, next) {
	res.tpl = {};
	res.tpl.error = [];

	return next();
});

// Serve assets files
app.use('/assets', express.static('assets'));

//Authentication
require('./routes/auth.js')(app);
passport_init(app);

app.mailer = nodemailer_init.mailer;


// Route to the needed logic
require('./routes/event.js')(app);
require('./routes/welcome.js')(app);
require('./routes/admin-events.js')(app);
require('./routes/admin-messages.js')(app);


// Handle port setting on Heroku and localhost
if(process.env.PORT === undefined){
	console.log("Detected running mode: Localhost");
	port = default_port;
}else{
	console.log("Detected running mode: Heroku");
	port = process.env.PORT;
}

app.listen(port, function () {
    console.log('Project *Budapest Connect* listening on port '+port+'!');
});
