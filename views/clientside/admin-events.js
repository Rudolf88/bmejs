/**
 * Created by Rudolf on 07/05/2016.
 */
//<% if (event._id != "") { %>
event_id = "<%= event._id %>";
//<% } %>


function save_btn(e){
    console.log("save_btn click");

    if(typeof event_id === 'undefined'){
        event_id = "";
    }

    var data = {
        name: $("#name").val(),
        img: $("#img").val(),
        date: $("#date").val(),
        time: $("#time").val(),
        details: $("#details").val(),
        place: $("#place").val()
    };

    var is_empty = Object.keys(data).every(function(k){return data[k] != ""});

    data._id = event_id;
    data.active = false;

    if(!is_empty){
        alert("Not every date is filled!");
        console.log(data);
        return;
    }else{
        console.log("Every date is filled :)");
    }

    console.log("Sending data to save: ");
    console.log(data);

    $.post("/admin/events/save", data, function(res){
        alert("Saved!\nDeatils: "+res);
        event_id = $.parseJSON(res);
        window.history.pushState('edit', 'Edit message', '/admin/events/edit/'+event_id);
    }).fail(function(err){
        alert("Save failed!");
        console.log(err)
    });
}


function activate_btn(e){
    console.log("activate_btn click");

    if(typeof event_id === 'undefined'){
        event_id = "";
    }
    if(event_id == ""){
        alert("First save the event!");
        return;
    }
    var old_text = $("#activate").html();
    var new_text;
    var new_state;

    if(old_text == "Aktiválás"){
        new_state = true;
        new_text = "Deaktiválás";
    }else{
        new_state = false;
        new_text = "Aktiválás";
    }

    var data = {
        _id: event_id,
        active: new_state
    };

    $.post("/admin/events/activate", data, function(res){
        alert(old_text+" sucess!\nDeatils: "+res);
        var button = $("#activate");
        button.html(new_text);
        button.toggleClass("btn-primary");
        button.toggleClass("btn-warning");
    }).fail(function(err){
        alert(old_text+" failed!");
        console.log(err)
    });
}


function delete_btn(e){
    console.log("delete_btn click");

    if(typeof event_id === 'undefined'){
        event_id = "";
    }
    if (event_id == ""){
        alert("Event not saved for sending!");
        return;
    }

    var data = {
        id: event_id
    };

    $.post("/admin/events/delete", data, function(res){
        alert("Event deleted!\nDeatils: "+res);
        window.location.replace("/admin/events");
    }).fail(function(err){
        alert("Delete failed!");
        console.log(err)
    });
}


$('.save_btn').on('click', save_btn);
$('.activate_btn').on('click', activate_btn);
$('.delete_btn').on('click', delete_btn);