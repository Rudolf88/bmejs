//<% if (msg._id != "") { %>
    msg_id = "<%= msg._id %>";
//<% } %>


function delete_btn(e){
    if(typeof msg_id === 'undefined'){
        msg_id = undefined;
    }
    if (msg_id == undefined || msg_id == null){
        alert("Message not saved for sending!");
        return;
    }

    var data = {
        id: msg_id
    };

    $.post("/admin/messages/delete", data, function(res){
        alert("Message deleted!\nDeatils: "+res);
        //$("#content").val("");
        //$("#subject").val("");
        //$("#name").val("");
        window.location.replace("/admin/messages");
    }).fail(function(err){
        alert("Delete failed!");
        console.log(err)
    });
}

function send_btn(e){
    if(typeof msg_id === 'undefined'){
        msg_id = undefined;
    }
    if (msg_id == undefined){
        console.log("Message not saved for sending!");
        return;
    }

    var data = {
        id: msg_id
    };

    console.log("Sending message: "+msg_id);

    $.post("/admin/messages/send", data, function(res){
        alert("Sent!\nDeatils: "+res);
    }).fail(function(err){
        alert("Sending failed!");
        console.log(err)
    });
}

function save_btn(e){
    if(typeof msg_id === 'undefined'){
        msg_id = undefined;
    }

    var data = {
        content: $("#content").val(),
        subject: $("#subject").val(),
        name: $("#name").val(),
        id: msg_id
    };

    console.log("Sending data to save: ");
    console.log(data);

    $.post("/admin/messages/save", data, function(res){
        alert("Saved!\nDeatils: "+res);
        msg_id = $.parseJSON(res);
        window.history.pushState('edit', 'Edit message', '/admin/messages/edit/'+msg_id);
    }).fail(function(err){
        alert("Save failed!");
        console.log(err)
    });
}



$('.send_btn').on('click', send_btn);
$('.save_btn').on('click', save_btn);
$('.delete_btn').on('click', delete_btn);