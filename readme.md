## 1. Feladat - Specifikáció készítése ##
Határidő: 2. hét vasárnap (február 28), 23:59

Egy elképzelt megrendelő feladatát specifikáljuk:

* Legalább 2 entitás típus (legalább egyiknél az összes CRUD funkció megvalósítása)
* Legalább 3 nézet (mindegyikhez mockup)
* Részletes specifikáció kell

Amit le kell adni:

* specifikáció pdfben (Mi az oldal célja, az egyes oldalon milyen adatok jelennek meg, milyen kapcsolat van az oldalak között)
* mockup vagy a specifikációban, vagy képként (pl. http://www.balsamiq.com/builds/mockups-web-demo/)

---------------

## A Budapest Connect honlapjának a fejlesztése. ##

**Entitások**

 * Hírlevél
 * Események

** Nézetek**

 - Welcome oldal:
    - Üdvözlő kép/szöveg
    - rólunk
    - helyszín
    - következő események
    - hírlevélre feliratkozás
    - üzenet küldés (emailben továbbítja valakinek)
 - Esemény oldal:
    - Egy esemény részleteinek megtekintése
 - Adminisztrációs felületek:
    - Körlevél üzenetek szerkesztése, küldése (markdown support, képekkel!)
    - Események szerkesztése