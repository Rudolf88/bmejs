/**
 * Created by Rudolf on 10/03/2016.
 */
var renderMW = require("../middleware/generic/render.js");
var event_listMW = require("../middleware/event/get_event_list.js");
var eventModel = require("../models/event.js");
var event_from_idMW = require("../middleware/event/get_event_by_id.js");
var adminPassMW = require("../middleware/generic/admin_pass.js");

var modifyMW = require("../middleware/event/modify_event.js");
var createMW = require("../middleware/event/create_event.js");
var deleteMW = require("../middleware/event/delete_event.js");
var activateMW = require("../middleware/event/activate_event.js");

var error_checkMW = require("../middleware/generic/error_check.js");

module.exports = function(app){

    var objrepo = {
        eventModel: eventModel
    };

    app.get('/admin/events/new',
        //adminPassMW(),
        event_from_idMW(objrepo),
        event_listMW(objrepo),
        renderMW(objrepo, "admin-event")
    );

    app.post('/admin/events/save',
        //adminPassMW(),
        event_from_idMW(objrepo),
        modifyMW(objrepo),
        createMW(objrepo),
        error_checkMW(objrepo)
    );

    app.post('/admin/events/delete',
        //adminPassMW(),
        event_from_idMW(objrepo),
        deleteMW(objrepo),
        error_checkMW(objrepo)
    );

    app.post('/admin/events/activate',
        //adminPassMW(),
        event_from_idMW(objrepo),
        activateMW(objrepo),
        error_checkMW(objrepo)
    );

    app.get('/admin/events/edit/:id',
        //adminPassMW(),
        event_from_idMW(objrepo),
        event_listMW(objrepo),
        renderMW(objrepo, "admin-event")
    );

    app.get('/admin/events',
        //adminPassMW(),
        event_listMW(objrepo),
        renderMW(objrepo, "admin-event-list")
    );
};