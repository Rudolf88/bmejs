/**
 * Created by Rudolf on 26/03/2016.
 */

var passport = require('passport');
var secret = require('../config/secret.js');

var logoutMW = require('../middleware/generic/logout');
var renderMW = require('../middleware/generic/render');

module.exports = function (app) {

    var objrepo = {

    };

    app.get('/auth/login',
        function(req, res, next){
            console.log("doing authentication");
            next();
        },
        passport.authenticate('facebook')
    );

    app.get('/login',
        renderMW(objrepo, 'login')
    );

    app.get('/auth/logout',
        logoutMW()
    );

    app.get(secret.facebookAuth.callbackURL,
        passport.authenticate('facebook', {
            successRedirect: '/',
            failureRedirect: '/auth/loginfail'
        }));

    app.get('/auth/loginfail',
        function(req, res){
            res.end("Login with Facebook failed!");
        });

};