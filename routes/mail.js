/**
 * Created by Rudolf on 29/03/2016.
 */

var subscribe = require('../middleware/subscription/subscribe_email');
var un_subscribe = require('../middleware/subscription/unsubscribe_email');
var send_mail = require('../middleware/generic/send_mail');

module.exports = function (app) {

    var obj_repo = {};

    app.get('/mail/subscribe/:mail',
        subscribe(obj_repo)
    );

    app.get('/mail/unsubscribe/:mail',
        un_subscribe(obj_repo)
    );

    app.get('/mail/send',
        send_mail(obj_repo)
    );

};
