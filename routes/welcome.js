/**
 * Created by Rudolf on 10/03/2016.
 */
var renderMW = require('../middleware/generic/render');
var eventListMW = require('../middleware/event/get_event_list');
var eventModel = require("../models/event.js");

module.exports = function(app){

    var objrepo = {
        eventModel: eventModel
    };

    app.get('/',
        eventListMW(objrepo),
        renderMW(objrepo, "welcome")
    );
};
