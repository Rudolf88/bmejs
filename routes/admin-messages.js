/**
 * Created by Rudolf on 10/03/2016.
 */

var modify_messageMW = require("../middleware/message/modify_message.js");
var create_messageMW = require("../middleware/message/create_message.js");
var delete_messageMW = require("../middleware/message/delete_message.js");

var get_message_listMW = require("../middleware/message/get_message_list.js");
var get_message_from_idMW = require("../middleware/message/get_message_from_id.js");
var send_messageMW = require("../middleware/subscription/send_message.js");

var renderMW = require("../middleware/generic/render.js");
var adminPassMW = require("../middleware/generic/admin_pass.js");
var error_checkMW = require("../middleware/generic/error_check.js");
var redirectMW = require("../middleware/generic/redirect.js");

var msgModel = require("../models/message.js");



module.exports = function (app) {

    var objrepo = {
        msgModel: msgModel,
        mailer: app.mailer
    };

    app.post('/admin/messages/delete',
        // adminPassMW(),
        get_message_from_idMW(objrepo),
        delete_messageMW(objrepo),
        error_checkMW(objrepo)
    );

    app.post('/admin/messages/send',
        // adminPassMW(),
        get_message_from_idMW(objrepo),
        send_messageMW(objrepo),
        error_checkMW(objrepo)
    );

    app.post('/admin/messages/save',
        //adminPassMW(),
        get_message_from_idMW(objrepo),
        modify_messageMW(objrepo),
        create_messageMW(objrepo),
        error_checkMW(objrepo)
    );

    app.get('/admin/messages',
        //adminPassMW(objrepo),
        get_message_listMW(objrepo),
        renderMW(objrepo, 'admin-messages-list')
    );


    app.get('/admin/messages/edit/:id',
        //adminPassMW(objrepo),
        get_message_listMW(objrepo),
        get_message_from_idMW(objrepo),
        renderMW(objrepo, 'admin-messages')
    );

    app.get('/admin/messages/new',
        //adminPassMW(objrepo),
        get_message_from_idMW(objrepo),
        get_message_listMW(objrepo),
        renderMW(objrepo, 'admin-messages')
    );

};