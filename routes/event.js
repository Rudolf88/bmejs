/**
 * Created by Rudolf on 10/03/2016.
 */
var event_from_idMW = require("../middleware/event/get_event_by_id.js");
var eventModel = require("../models/event.js");
var renderMW = require("../middleware/generic/render.js");

module.exports = function(app){

    var objrepo = {
        eventModel: eventModel
    };

    app.get('/event/:id',
        event_from_idMW(objrepo),
        renderMW(objrepo, "event")
    );
};